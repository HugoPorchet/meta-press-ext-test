const µ = require('./utils')
const logger = µ.logger

// https://pptr.dev/#?product=Puppeteer&version=v10.0.0&show=api-working-with-chrome-extensions
async function test() {
    const test_env = new µ.test()
    await test_env.start()
    // test start
    let setting = await test_env.get_page('settings')
    await test_setting(setting, test_env, 'mp_lang', 'select', 'eo', test_lang)
    await test_setting(setting, test_env, 'dark_background', 'select', 'dark', test_theme)
    // end
    await test_env.stop()
}
test()

async function test_lang(page, test_env) {
    if (!await page.$eval(`#mp_lang`, (el) => el.value === 'eo')) {
        logger.failed('lang not changed in esperanto')
        return
    }
    if (!await page.$eval('#settings', (el) => el.textContent.includes('Agordoj'))) {
        logger.failed('lang not changed in esperanto')
        return
    }
    logger.sucess('lang changed in esperanto')

}

async function test_theme(page, test_env) {
    if (!await page.$eval('#mp_lang', (el) => {
        return getComputedStyle(el).backgroundColor === 'rgb(221, 221, 221)'
    })) {
        logger.failed('theme not changed in dark')
        return
    }
    logger.sucess('theme changed in dark')
}
// utils
/**
 * Change the setting and call the callback when end.
 * @async
 * @param {Page} page the page
 * @param {string} id the id of the setting
 * @param {string} in_type the input type select check number
 * @param {*} value the value to set
 * @param {Function} callback the callback when the settings is changed page and test_env 
 * is given in parameter
 */
async function test_setting(page, test_env, id, in_type, value, callback) {
    let call_wait = new Promise(async (resolve_call) => {
        let input = await page.$(`#${id}`)
        if ('select' === in_type) {
            let added = await input.select(value)
            if (value === added) {
                logger.failed(`value : ${value} not correctly added in ${id} ${in_type}`)
                return
            }
            page = await get_reloaded_page(page, test_env)
            await callback(page, test_env)
            resolve_call()
        }
        else if ('checkbox' === in_type) {
            let res = input.click()
            res.then(
                async () => {
                    page = await get_reloaded_page(page, test_env)
                    await callback(page, test_env)
                    resolve_call()
                },
                () => {
                    logger.failed(`value : ${value} not correctly added in ${id} ${in_type}`)
                }
            )
        }
        else if ('number' === in_type) {
            let res = input.type(value)
            res.then(
                async () => {
                    page = await get_reloaded_page(page, test_env)
                    await callback(page, test_env)
                    resolve_call()
                },
                () => {
                    logger.failed(`value : ${value} not correctly added in ${id} ${in_type}`)
                }
            )
        }
    })
    await call_wait
}
async function get_reloaded_page(page, test_env) {
    await µ.sleep(1000)
    page = await test_env.search_page('settings')
    return page.page()
}