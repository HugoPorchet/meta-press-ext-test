const fs = require('fs')
const { Console } = require('console')
const puppeteer = require('puppeteer')

// date
let date = new Date()
date = `${date.getFullYear()}-${date.getMonth()}-${date.getDay()}_`
    + `${date.getHours()}:${date.getMinutes()}`
// result exist
if (!fs.existsSync('./result'))
    fs.mkdirSync('./result')
// create logger
const output = fs.createWriteStream(`./result/stdout-${date}.log`)
const stderr = fs.createWriteStream(`./result/stderr-${date}.log`)
const logger = new Console({ stdout: output, stderr: stderr })

class Test {
    constructor() {
        this.browser = undefined
        this.welcome_page = undefined
    }
    async search_page(name) {
        const targets = this.browser.targets()
        return await targets.find(
            (target) => target.type() === 'page'
                && target.url().includes(name)
        )
    }
    /**
     * Start the browser.
     * @async
     */
    async start() {
        // launch browser
        const path_to_extension = require('path').join(__dirname, '../../meta-press-ext')
        this.browser = await puppeteer.launch({
            'headless': false,
            slowMo: 50,
            args: [
                `--disable-extensions-except=${path_to_extension}`,
                `--load-extension=${path_to_extension}`,
            ],
        })
        let res = new Promise((resolve, rejects) => {
            this.browser.on('targetcreated', async () => {
                let val = await this.search_page('welcome.html')
                resolve(await val.page())
            })
        })
        this.welcome_page = await res
    }
    /**
     * Stop the browser.
     * @async
     */
    async stop() {
        await this.browser.close()
    }
    /**
     * Create a new tab with the href in welcome.html with the id (name given)
     * and return if id is not found in welcome puppeteer throw an error.
     * @async
     * @param {string} name the page name
     * @returns {Page}
     */
    async get_page(name) {
        if (typeof (this.welcome_page) === 'undefined')
            throw new Error('Browser not started')
        await this.welcome_page.$eval(`#${name}`,(elt) => {
            elt.style.display = 'block'
            return elt
        })
        let button = await this.welcome_page.$(`#${name}`)
        if (button === null)
            throw new Error('Button for got to the given page not found in welcome')
        await button.click()
        let targeted_page = await this.search_page(name)
        return await targeted_page.page()
    }
}


module.exports = {
    'logger': {
        'info': logger.info,
        'log': logger.log,
        'warn': logger.warn,
        'error': logger.error,
        'test': (txt) => {
            logger.log('[TEST] ' + txt)
        },
        'sucess': (txt) => {
            logger.log('[SUCCESS] ' + txt)
        },
        'failed': (txt) => {
            logger.log('[ERROR] ' + txt)
            logger.error('[ERROR] ' + txt)
        }
    },
    'test': Test,
    'sleep': (delay=50) => new Promise((resolve) => setTimeout(resolve, delay)),
    'diff_file': (path_1, path_2) => {
        let f_1 = fs.readFileSync(path_1)
        let f_2 = fs.readFileSync(path_2)
        return f_1.toString('binary') === f_2.toString('binary')
    },
    // https://github.com/puppeteer/puppeteer/blob/main/examples/custom-event.js
    'listenFor': function (page, type, func) {
        return page.evaluate((type) => {
            document.addEventListener(type, () => {
                window[func]({ type })
            })
        }, type)
    },
    /**
     * Iterate over the pagination and call the callback after a page change.
     * @todo make possible the content script to be ex for iterate in listjs
     * @param {string} id The id of the list creation
     * @param {Function} callback The callback for applie in all the result
     */
    'forListjs': async function (page, id, callback) {
        let toclick = await page.$(`#${id} .pagination .active + li`)
        logger.log(toclick)
        while (toclick !== null) {
            await this.sleep(200)
            await toclick.click()
            await callback(page, id)
            toclick = await page.$(`#${id} .pagination .active + li`)
        }
    }
}

