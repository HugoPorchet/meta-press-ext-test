const µ = require('./utils')
const logger = µ.logger
const path = require('path')
const fs = require('fs')

// https://pptr.dev/#?product=Puppeteer&version=v10.0.0&show=api-working-with-chrome-extensions
async function test() {
    const test_env = new µ.test()
    await test_env.start()
    // test start
    let index = await test_env.get_page('index')
    // https://github.com/puppeteer/puppeteer/blob/main/examples/custom-event.js
    // Define a window.onCustomEvent function on the page.
    await index.exposeFunction('onCustomEvent', async () => {})
    await index.exposeFunction('export_download', async () => {})
    await µ.listenFor(index, 'tags_src_load', 'onCustomEvent')
    // work
    await test_menu(index)
    await test_cherrypick(index, 'collection', 'WikiMedia')
    await test_cherrypick(index, 'name', 'ANSA')
    await test_src_button_panel(index)
    await test_search_block_empty_selec(index)
    // RSS
    let rss_import = await index.$('#mp_import_RSS')
    let export_rss = await index.$('#mp_export_search_RSS')
    logger.test('Import export RSS')
    await test_import_export(index, 'XML', 'mp_RSS.rss', rss_import, export_rss)
    // ATOM
    // atom is slow and can make other test crash with timeout
    await (await index.$('.menu_export .blog-nav-item.drop_btn')).click()
    let atom_import = await index.$('#mp_import_ATOM')
    let export_atom = await index.$('#mp_export_search_ATOM')
    export_atom['display_test'] = async (index) => {
        await (await index.$('#drop_down_export .drop_btn')).focus()
    }
    logger.test('Import export ATOM')
    await test_import_export(index, 'XML', 'mp_ATOM.atom', atom_import, export_atom)
    // JSON
    // import export no the same result
    let json_import = await index.$('#mp_import_JSON')
    let export_json = await index.$('#mp_export_search_JSON')
    export_json['display_test'] = async (index) => {
        await (await index.$('#drop_down_export .drop_btn')).focus()
    }
    logger.test('Import export JSON')
    await test_import_export(index, 'JSON', 'mp_JSON.json', json_import, export_json)
    // CSV
    let csv_import = await index.$('#mp_import_CSV')
    let export_csv = await index.$('#mp_export_search_CSV')
    export_csv['display_test'] = async (index) => {
        await (await index.$('#drop_down_export .drop_btn')).focus()
    }
    logger.test('Import export CSV')
    await test_import_export(index, 'CSV', 'mp_CSV.csv', csv_import, export_csv)
    logger.test('Exemple debug de parcours de list js')
    await (await index.$('.menu_export .blog-nav-item.drop_btn')).click()
    await test_result_needed(index, 'findings', () => {}, () => {})
    // end
    test_env.stop()
}
test()

async function test_menu(index) {
    let tag_handle = await index.$('#tags_handle')
    // close if open
    if (await index.$eval('#tags_row', (elt) => elt.style.display === 'block')) {
        await tag_handle.click()

    }
    logger.test('testing advanced search display')
    // open for test
    await tag_handle.click()
    let open = true
    open = open && await index.$eval('#tags_row', (el) => el.style.display === 'block')
    if (!open)
        logger.error('tags row not open')
    open = open && await index.$eval('#tags_2nd_row', (el) => el.style.display === 'block')
    if (!open) {
        logger.failed('open advanced settings')
        return
    }
    logger.sucess('open advanced settings')

    // panel src collection test
    logger.test('subtest: testing view select panel display')
    let view_source_button = await index.$('#view_select_src')
    await view_source_button.click()
    open = true
    open = open && index.$eval('#list_src', (el) => el.style.display === 'block')
    if (!open) {
        logger.failed('open view select panel')
        return
    }
    logger.sucess('open view select panel')

    try {
        await view_source_button.click()
    } catch (err) {
        logger.failed('close view select panel : close source button')
        logger.error(err)
        return
    }
    open = true
    open = open && index.$eval('#list_src', (el) => el.style.display === 'none')
    if (!open) {
        logger.failed('close view select panel : view source')
        return
    }
    logger.sucess('close view select panel : view source')

    await view_source_button.click()
    let close_sel_src = await index.$('#close_src')
    try {
        await close_sel_src.click()
    } catch (err) {
        logger.failed('close view select panel : close source button')
        logger.error(err)
        return
    }
    open = true
    open = open && index.$eval('#list_src', (el) => el.style.display === 'none')
    if (!open) {
        logger.failed('close view select panel : close source button')
        return
    }
    logger.sucess('close view select panel : close source button')

    // close all
    await view_source_button.click()
    await tag_handle.click()
    open = true
    open = open && await index.$eval('#tags_row', (el) => el.style.display === 'none')
    if (!open)
        logger.error('tags row is open')
    open = open && await index.$eval('#tags_2nd_row', (el) => el.style.display === 'none')
    if (!open) {
        logger.failed('close advanced settings')
        return
    }
    logger.sucess('close advanced settings')
    logger.test('end advanced search display')
}

async function test_cherrypick(index, name, tag) {
    await show_tags(index)
    // test cherry
    logger.test('testing cherrypick ' + name)
    let tags_name = ['src_type', 'lang', 'res_type', 'themes', 'tech', 'country',
        'name', 'collection']
    let tags = {}
    let cherry = await index.$(`#tags_${name} + div + input`)
    for (const i of tags_name)
        tags[i] = await index.$(`#tags_${i}`)

    // execute test
    await cherry.focus()
    await cherry.type(tag)

    await cherry.press('Enter')


    if (await index.$(`#tags_${name} option`) === null) {
        logger.failed('cherrypick ' + tag)
        return
    }

    let success = true
    for (const i of tags_name) {
        if (i === name) continue
        success = success && await tags[i].$('option') === null
        success = success && await index.$eval(`#tags_${i}`, (el) => el.disabled === true)
        if (!success) {
            logger.failed(`for ${name} choice ${i} is not disabled`)
            break
        }
    }
    logger.sucess(`cherrypick ${name} is passed with ${tag}`)

    let rm_sel = await index.$$(`#tags_${name} + div button`)
    for (const i of rm_sel)
        await i.click()
}

async function test_src_button_panel(index) {
    // initial condition
    await select_src(index, ['ANSA', '01net'])
    await show_tags(index)
    let view_source_button = await index.$('#view_select_src')
    await view_source_button.click()

    logger.test('src listing command add')
    if (!await index.$eval('#cur_tag_sel_nb', (el) => el.textContent >= 2)) {
        logger.failed('add src not work')
        return
    }
    logger.sucess('src add worked')

    await (await index.$('#v_select')).click()
    let selected = await index.$$('.selected')
    for (const i of selected) {
        await (await i.$('.src_list_del')).click()
        if ((await index.$$('.selected')).length >= selected.length) {
            logger.failed('remove src not worked')
            return
        }
    }
    logger.sucess('src remove worked')
}

async function test_search_block_empty_selec(index) {
    // remove all the src in selection
    logger.test('mp_submit disabled with no source')
    await select_src(index, ['ANSA'])
    await show_tags(index)
    let view_source_button = await index.$('#view_select_src')
    await view_source_button.click()

    await (await index.$('#v_select')).click()
    await (await index.$('.src_list_del')).click()


    if (!await index.$eval('#mp_submit', (el) => el.disabled)) {
        logger.failed('mp_submit is not disabled')
        return
    }
    logger.sucess('mp_submit disabled')
}

/**
 * Test the import and export if differencies between the file raise error.
 * @async
 * @param {Page} index The current page
 * @param {string} f_type the type of import in upper case
 * @param {string} file_imp The name of the file without /
 * @param {ElementHandle} import_click The button displayed to click for import
 * @param {ElementHandle} export_click The button displayed to click for export
 * can have display_test function for show (index is passed)
 */
async function test_import_export(index, f_type, file_imp, import_click, export_click) {
    let fileChooser = index.waitForFileChooser()
    let to_import_path = process.cwd() + `/import/${file_imp}`
    await import_click.click();
    await (await fileChooser).accept([to_import_path])
    await µ.listenFor(index, 'result_load', 'export_download')
    // delete old file
    fs.readdirSync('./export').forEach(file => { fs.rm('./export/' + file, () => { }) })
    // https://help.apify.com/en/articles/1929322-handling-file-download-with-puppeteer
    await index._client.send('Page.setDownloadBehavior', {
        behavior: 'allow',
        downloadPath: path.resolve('./export')
    })
    // await µ.sleep(100000)
    if (export_click.display_test) await export_click.display_test(index)
    await µ.sleep()
    await export_click.click()
    await µ.sleep(6000)
    let name = fs.readdirSync('./export')[0]
    if (typeof (name) === 'undefined' || µ.diff_file(to_import_path, process.cwd() + '/export/' + name))
        logger.failed(`import or export ${f_type} not work`)
    else
        logger.sucess(`import and export ${f_type} work`)
}

/**
 * Import a search call the action and iterate over the listjs pagination for the result.
 * @async
 * @todo make possible the content script to be ex for iterate in listjs
 * @param {Page} page The current page
 * @param {string} id the id of the div who list has been create
 * @param {Function} action execution of the functionnality
 * @param {Function} validator check if the functionnality is good in iterate over result
 */
async function test_result_needed(page, id, action, validator) {
    await (await page.$('.menu_export .blog-nav-item.drop_btn')).click()
    let csv_import = await page.$('#mp_import_JSON')
    let fileChooser = page.waitForFileChooser()
    let to_import_path = process.cwd() + `/import/mp_JSON.json`
    await csv_import.click();
    await (await fileChooser).accept([to_import_path])
    await µ.listenFor(page, 'result_load', 'export_download')
    // result_list
    await action(page)
    await µ.forListjs(page, id, validator)
}

// utils
/**
 * Select the src_names with the v_panel.
 * @async
 * @param {Page} index The html page
 * @param {Array} src_names The name to select
 */
async function select_src(index, src_names) {
    await unselec_all_tags(index)
    let tag_handle = await index.$('#tags_handle')
    if (await index.$eval('#tags_row', (elt) => elt.style.display === 'none')) {
        await tag_handle.click()

    }
    let tags_name = ['src_type', 'lang', 'res_type', 'themes', 'tech', 'country',
        'name', 'collection']
    let cherry = await index.$(`#tags_country + div + input`)

    await cherry.focus()
    await cherry.type('at')
    await cherry.press('Enter')
    for (const i of tags_name)
        if (i !== 'country') {
            let buttons = await index.$$(`#tags_${i} + div button`)
            for (const j of buttons)
                await buttons.click()
        }

    if (await index.$eval('#list_src', (elt) => elt.style.display === 'none')) {
        let view_source_button = await index.$('#view_select_src')
        await view_source_button.click()

    }
    await (await index.$('#v_select')).click()

    let to_rm = await index.$$('.selected .src_list_del')
    for (const i of to_rm)
        if (i && i['click'])
            await i.click()

    await (await index.$('#v_all')).click()
    let search = await index.$('#mp_v_src_search')
    let search_clear = await index.$('#mp_clear_v_src_search')
    let selec = '.src_item .v_list_item .src_list_add'
    for (const i of src_names) {
        await search_clear.click()
        await search.type(i)
        await search.press('Enter')

        if (await index.$eval(selec, (el) => el.style.display === 'none'))
            continue
        let to_add = await index.$(selec)
        await to_add.click()
    }
    await search_clear.click()
    await tag_handle.click()
}

/**
 * Unselect all tags in the page.
 * @async
 * @param {Page} index Meta-Press.es page
 */
async function unselec_all_tags(index) {
    let to_click = await index.$$('choices__button')
    if (to_click.length === 0) return
    for (const i of to_click)
        await i.click()
}

/**
 * Show the tags menu whitout v_panel for the sources.
 * @async
 * @param {Page} index Meta-Press.es page
 */
async function show_tags(index) {
    let tag_handle = await index.$('#tags_handle')
    if (await index.$eval('#tags_row', (elt) => elt.style.display === 'none')) {
        await tag_handle.click()

    }
}

async function get_reloaded_page(page, test_env) {
    await µ.sleep(1000)
    page = await test_env.search_page('metapress')
    return page.page()
}