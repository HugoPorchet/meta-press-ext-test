all: metapress setting

metapress:
	@echo "Start Metapress.es default page"
	node ./test/metapress_test.js

setting:
	@echo "Start settings page"
	node ./test/settings.js

.PHONY: metapress